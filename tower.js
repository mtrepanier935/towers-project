let mainContainer = document.getElementById('mainContainer')

let clickMode = "pickUp"
 if (clickMode === "pickUp") {
    mainContainer.addEventListener('click', pickUp)
  } else {
    maincontainer.addEventListener('click', putDown)
  }

function pickUp(event){
    let selectedTower = event.target
      if(!selectedTower.className.includes('tower')) {
          return 
      }
      selectedTower.lastElementChild.classList.add("selected")
      clickMode = "putDown"
      mainContainer.removeEventListener("click", pickUp)
      mainContainer.addEventListener('click', putDown)
}

function putDown(event){
    let towerSecondClick = event.target
    let selectedDisk = document.querySelector('.selected')

    if(!towerSecondClick.className.includes('tower')){
        return
    }
    if(towerSecondClick.childElementCount === 0 ){
      towerSecondClick.appendChild(selectedDisk)
      selectedDisk.classList.remove('selected')
      clickMode = 'pickUp'
      mainContainer.removeEventListener('click', putDown)
      mainContainer.addEventListener('click', pickUp)
      console.log(towerSecondClick.lastElementChild.dataset["value"])
    }else if(towerSecondClick.lastElementChild.dataset["value"]<selectedDisk.dataset["value"]){
      alert("Can't move there")
      return
    }else{
      towerSecondClick.appendChild(selectedDisk)
      selectedDisk.classList.remove('selected')
      clickMode = 'pickUp'
      mainContainer.removeEventListener('click', putDown)
      mainContainer.addEventListener('click', pickUp)
    }
    let endTower = document.getElementById('end').childElementCount
    if(endTower === 4 && !document.getElementById('end').lastChild.className.includes('selected') ){
          alert('You WIN!')
    }
    
}

